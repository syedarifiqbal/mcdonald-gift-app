@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            @foreach($records as $day => $record)
                                <li class="nav-item">
                                    <a class="nav-link {{ $loop->first? 'active': '' }}" id="pills-{{ $day }}-tab" data-toggle="pill" href="#pills-{{ $day }}" role="tab" aria-controls="pills-home" aria-selected="true">Day {{ $day }}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content" id="pills-tabContent">

                            @foreach($records as $day => $gifts)
                                <div class="tab-pane fade {{ $loop->first? 'active show': '' }}" id="pills-{{ $day }}" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Gift</th>
                                            @if($loop->index > 8)
                                                <th>Participated at</th>
                                            @else
                                                <th>Won at</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($gifts as $gift)
                                            @foreach($gift as $winner)
                                                <tr>
                                                    <td>{{ $winner->id }}</td>
                                                    <td>{{ $winner->name }}</td>
                                                    <td>{{ $winner->email }}</td>
                                                    <td>{{ $winner->phone }}</td>
                                                    @if($loop->index > 8)
                                                    @else
                                                        <td>{{ $winner->gift->gift }}</td>
                                                    @endif
                                                    <td>{{ $winner->created_at->format('m/d/Y h:i:s A') }}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @if($losers->has($day))
                                        <h1 class="text-danger">Losers</h1>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Lost at</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($losers[$day] as $loser)
                                                <tr>
                                                    <td>{{ $loser->id }}</td>
                                                    <td>{{ $loser->name }}</td>
                                                    <td>{{ $loser->email }}</td>
                                                    <td>{{ $loser->phone }}</td>
                                                    <td>{{ $loser->created_at->format('m/d/Y h:i:s A') }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
