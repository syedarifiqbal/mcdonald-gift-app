<div class="container">
    <div class="row">
        <div class="col">
            <div class="raffle-wraper">
                <div class='raffle-container'>
                    <div class='rafflebox'>
                        <div class='pointer'></div>
                        <div class='boxwrapper'>
                            <ul class='giftwrapper'>
                                <li class='gift'>
                                    <img src='{{ asset('images/gift.svg') }}'>
                                    <p class="d-none">Gift 1</p>
                                </li>
                                <li class='gift'>
                                    <img src='{{ asset('images/gift.svg') }}'>
                                    <p class="d-none">Gift 2</p>
                                </li>
                                <li class='gift'>
                                    <img src='{{ asset('images/gift.svg') }}'>
                                    <p class="d-none">Gift 3</p>
                                </li>
                                <li class='gift'>
                                    <img src='{{ asset('images/gift.svg') }}'>
                                    <p class="d-none">Gift 4</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <button class='button' id="raffle">PARTICIPAR</button>
            </div>
            <?php
            $lastGift = App\Models\Gift::orderBy('id', 'desc')->limit(1)->first();
            $lastGiftEndTime = now()->setTimeFromTimeString(substr($lastGift->date, 0, 10) . ' ' . $lastGift->today_end_time);
            ?>
            @if( now() > $lastGiftEndTime )
                <h1><a href="#" style="color: #fff">El concurso ha finalizado. <br> Clic aqu&iacute; para ver las reglas.</a></h1>
                <style>.hide-on-final { display: none !important; }</style>
            @else
                <button class='button mb-5' id="participant">PARTICIPAR</button>
            @endif

            <div class="modal fade" data-backdrop="static" id="modal" tabindex="-1" role="dialog">
                <form action="#" id="visitorInfo" class="modal-dialog" role="document">
                    <input type="hidden" id="isLastThreeDays" value="true">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Registrate para participar por premios diaros durante 12 días:</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="formBody">
                                <div class="form-group">
                                    <label for="name">Nombre completo</label>
                                    <input type="text" class="form-control" id="name"  required>
                                    <div class="invalid-feedback">Nombre completo es requerido.</div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo electr&oacute;nico</label>
                                    <input type="email" class="form-control" id="email" required>
                                    <div class="invalid-feedback">Correo electr&oacute;nico es requerido.</div>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Celular</label>
                                    <input type="tel" class="form-control" id="phone" required>
                                    <div class="invalid-feedback">Celular es requerido.</div>
                                </div>
                                <div class="form-group">
                                    <input class="form-check-input" type="checkbox" name="agree" value="agree" id="SecondCheck" required="true" style="margin-left: 0">
                                    <label class="form-check-label" for="SecondCheck" style="margin-left: 1.4em"> Soy mayor de 21 años</label>
                                </div>
                                <div class="form-group" style="margin-bottom: 0">
                                    <input class="form-check-input" type="checkbox" name="agree" value="agree" id="firstCheck" required="true" style="margin-left: 0">
                                    <label class="form-check-label" for="firstCheck" style="margin-left: 1.4em"> Certifico que he leído y acepto los <a href="reglas.html" target="_blank" style="color: #fff; text-decoration:underline">términos y condiciones de la promoción</a>.</label>
                                </div>
                            </div>
                            <div id="loading" style="display:none;">
                                <div id="preloader"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">PARTICIPAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>