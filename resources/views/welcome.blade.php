<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>12 días de NavidApp con McDonald's</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="images/favicon.png" type="image/png">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">

    <link rel="stylesheet" href="/css/app.css">

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TRSJXTD');</script>
    <style>
        .modal-content {
            background-image: url({{ asset('/images/bg-for-all-popups.jpg') }});
        }

        .swal2-popup.success-modal {
            background-image: url({{ asset('/images/bg-for-win-message.jpg') }}) !important;
        }
    </style>
</head>
<body>

<div id="app">

    <div class="navbar navbar-expand-sm bsnav">

        <div class="collapse navbar-collapse ">
            <ul class="navbar-nav navbar-mobile mr-0">
            <!--{{--@auth--}}
                    <li class="nav-item active"><a class="nav-link" href="{{ route('gifts.participant') }}">Participa</a></li>
                {{--@endauth--}}-->
                <li class="nav-item active"><a class="nav-link" href="#participa">Participa</a></li>
                <li class="nav-item"><a class="nav-link" href="#premios">Ver premios diarios</a></li>
                <li class="nav-item"><a class="nav-link venobox" href="reglas.html" data-vbtype="iframe">Reglas</a></li>
            </ul>
        </div>

        <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
        <a class="navbar-brand" href="#"><img src="images/logo.svg"></a>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-8 offset-2 text-center">
                    <img src="images/logo-promo.png" class="img-fluid mx-auto">

                    <div class="clearfix"></div>
                    <?php
                    $ld = $lastGift->date->modify('+1 day');
                    ?>
                    @if(now() >= $ld)
                        <h1><a href="#" style="color: #fff">El concurso ha finalizado. <br> Clic aqu&iacute; para ver las reglas.</a></h1>
                    @else
                        @if(!$timeEnded)
                            <a class='button hide-on-final' role="button" href="#participa" style="position: initial; display: initial">PARTICIPA</a>
                            <br><br>
                            <h1 class="mt-5 hide-on-final">Premio del día</h1>
                            <div class="row hide-on-final">
                                <div class="col-8 offset-2 text-center">
                                    <!-- dynamic: main image of gift of the day -->
                                    @if($todayGift)
                                        <img src="images/{{ $todayGift->img }}" class="img-fluid mx-auto">
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>

            <div class="row" id="participa">
                <div class="col-12 text-center">
                    @if($timeEnded)
                        <h1 class="text-center">Ya los premios se acabaron por hoy. Favor de visitarnos mañana.</h1>
                        @if($upcomingGift)
                            <img src="{{ asset("images/$upcomingGift->img") }}" class="img-fluid mx-auto mb-3" width="250">
                        @endif
                    @else
                        <!-- 'the more participations, the more opportunities you have to win final (3) prizes' copy-->
                        <h3 class="my-3 hide-on-final">Entre más participes más oportunidades tienes de ganar premios diarios</h3>

                        <!-- dynamic: raffle script -->
                        <div class="my-5">

                            @include('raffle')

                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section id="premios" class="mb-5">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 text-center">
                    <img src="images/star.png" class="img-fluid mx-auto mb-3 star">
                </div>
            </div>

            <!--day 1 and 2-->
            {{--{{ dd($row1) }}--}}
            <div class="row">
                <div class="col-3 col-sm-4"></div>
                @foreach($row1 as $gift)
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset("images/$gift->img") }}" class="img-fluid mx-auto{{ $gift->isExpiredClass() }}">
                    </div>
                @endforeach
            </div>

            <!--day 3, 4 and 5-->
            <div class="row">
                <div class="col-2 col-sm-3 col-fix"></div>
                <style>
                    @media (max-width: 575px) {
                        .col-fix {
                            flex: 0 0 12.267% !important;
                            max-width: 12.267% !important;
                        }
                    }
                </style>

                @foreach($row2 as $gift)
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset("images/$gift->img") }}" class="img-fluid mx-auto{{ $gift->isExpiredClass() }}">
                    </div>
                @endforeach
            </div>

            <!--day 6, 7, 8 and 9-->
            <div class="row">
                <div class="col-sm-2"></div>
                @foreach($row3 as $gift)
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset("images/$gift->img") }}" class="img-fluid mx-auto{{ $gift->isExpiredClass() }}">
                    </div>
                @endforeach
            </div>

            <!--day 10, 11 and 12-->
            <div class="row">
                <div class="col-sm-10 offset-sm-1 text-center">
                    <div class="row">
                        @foreach($row4 as $gift)
                            <div class="col-4 col-sm-4 text-center">
                                <img src="{{ asset("images/$gift->img") }}" class="img-fluid mx-auto{{ $gift->isExpiredClass() }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="legal">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-justify">
                    <p class="mt-3">Nada que comprar para participar. La promocion finaliza el 24 de diciembre de 2018. Puede participar cualquier persona mayor de 21 años, residente legal del Estado Libre Asociado de Puerto Rico. Para más detalles y restricciones ver reglas oficiales haciendo <a class="venobox" href="reglas.html" data-vbtype="iframe">clic aquí</a>. Auspicia Arcos Dorados.</p>
                </div>
            </div>
        </div>
    </section>

    <div class="bsnav-mobile">
        <div class="bsnav-mobile-overlay"></div>
        <div class="navbar"></div>
    </div>

</div>
<script src="/js/app.js"></script>
</body>
</html>