<div class="container">
    <div class="row">
        <div class="col">
            @if($todayGift->day < 10)
                <div class="raffle-wraper">
                    <div class='raffle-container'>
                        <div class='rafflebox{{ $timeEnded? ' disabled':'' }}'>
                            @if($timeEnded)
                                <div id="closeMessage">
                                    <h1>El concurso ha finalizado por hoy. Favor de visitarnos a las {{$todayGift->tomorrow_start_at}}</h1>
                                    <!--Gifts have ended for today, visit us tomorrow at-->
                                </div>
                            @endif
                            @if(!$timeEnded)
                                <div class='pointer'></div>
                            @endif
                            <div class='boxwrapper'>
                                <ul class='giftwrapper'>
                                    <li class='gift'>
                                        <img src='{{ asset('images/gift.svg') }}'>
                                        <p class="d-none">Gift 1</p>
                                    </li>
                                    <li class='gift'>
                                        <img src='{{ asset('images/gift.svg') }}'>
                                        <p class="d-none">Gift 2</p>
                                    </li>
                                    <li class='gift'>
                                        <img src='{{ asset('images/gift.svg') }}'>
                                        <p class="d-none">Gift 3</p>
                                    </li>
                                    <li class='gift'>
                                        <img src='{{ asset('images/gift.svg') }}'>
                                        <p class="d-none">Gift 4</p>
                                    </li>
                                    {{--<li class='gift'>--}}
                                    {{--<img src='{{ asset('images/gift.svg') }}'>--}}
                                    {{--<p class="d-none">Gift 5</p>--}}
                                    {{--</li>--}}
                                    {{--<li class='gift'>--}}
                                    {{--<img src='{{ asset('images/gift.svg') }}'>--}}
                                    {{--<p class="d-none">Gift 6</p>--}}
                                    {{--</li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if(!$timeEnded)
                        <?php $giftEndTime = now()->setTimeFromTimeString(substr($todayGift->date, 0, 10) . ' ' . $todayGift->time_to); ?>
                        <?php $giftStartTime = now()->setTimeFromTimeString(substr($todayGift->date, 0, 10) . ' ' . $todayGift->time_from); ?>
                        @if( now() < $giftStartTime )
                            <button class='button' id="raffle">PARTICIPA</button>
                            <!--cursor: not-allowed; opacity: 0.7;" disabled="disabled"-->
                        @else
                            <button class='button' id="raffle">PARTICIPA</button>
                        @endif
                    @endif
                </div>
            @else
                <?php
                $lastGift = App\Models\Gift::orderBy('id', 'desc')->limit(1)->first();
                $lastGiftEndTime = now()->setTimeFromTimeString(substr($lastGift->date, 0, 10) . ' ' . $lastGift->today_end_time);
                ?>
                @if( now() > $lastGiftEndTime )
                    <a href="#">El concurso ha finalizado. Clic aqu铆 para ver las reglas.</a>
                    <!--The contest had ended. Click here to view the rules.-->
                @else
                    <?php $giftEndTime = now()->setTimeFromTimeString(substr($todayGift->date, 0, 10) . ' ' . $todayGift->time_to); ?>
                    <?php $giftStartTime = now()->setTimeFromTimeString(substr($todayGift->date, 0, 10) . ' ' . $todayGift->time_from); ?>
                    @if( now() > $giftEndTime && now() < $giftStartTime )
                        <button class='button' id="participant">PARTICIPA</button>
                        <!--cursor: not-allowed; opacity: 0.7;" disabled="disabled"-->
                    @else
                        <button class='button' id="participant">PARTICIPA</button>
                    @endif
                @endif
            @endif
            @if(!$timeEnded)
                <div class="modal fade" data-backdrop="static" id="modal" tabindex="-1" role="dialog">
                    <form action="#" id="visitorInfo" class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registrate para participar por premios diarios durante 12 d铆as:</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="formBody">
                                    <div class="form-group">
                                        <label for="name">Nombre completo</label>
                                        <input type="text" class="form-control" id="name" required>
                                        <div class="invalid-feedback">Nombre completo es requerido.</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Correo electr贸nico</label>
                                        <input type="email" class="form-control" id="email"  required>
                                        <div class="invalid-feedback">Correo electr贸nico es requerido.</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Celular</label>
                                        <input type="tel" class="form-control" id="phone"  required>
                                        <div class="invalid-feedback">Celular es requerido.</div>
                                    </div>
                                </div>
                                <div id="loading" style="display:none;">
                                    <div id="preloader"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">PARTICIPAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        </div>
    </div>
</div>