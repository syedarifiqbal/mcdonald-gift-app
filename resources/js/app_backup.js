
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../js/bsnav.min');
require('../js/venobox.min');
require('./custom');

import swal from 'sweetalert2';

let giftamount = 4;
let gw = $('.gift').outerWidth(true);
let giftcenter = gw/2;
let cycle = 7;

let containercenter = $('.boxwrapper').outerWidth(true)/2;
for(let i = 0; i <=giftamount; i++)
{
    let $giftwrapper = $('.giftwrapper');
    let giftduplicate = $giftwrapper.children().clone(true,true);
    $giftwrapper.append(giftduplicate);
}

$('#raffle').click(function(){
    $(this).prop('disabled', true).css({'opacity': 0.5, 'cursor' : 'not-allowed'}).hide();
    const name = window.localStorage.getItem('name');
    const email = window.localStorage.getItem('email');
    const phone = window.localStorage.getItem('phone');
    if (name && email && phone){
        getGiftAndRunRaffle(name, email, phone);
    }
    else
        $('#modal').modal('show');
});

$('#participant').click(function(){
    $(this).prop('disabled', true).css({'opacity': 0.5, 'cursor' : 'not-allowed'});
    const name = window.localStorage.getItem('name');
    const email = window.localStorage.getItem('email');
    const phone = window.localStorage.getItem('phone');
    if (name && email && phone){
        getGiftAndRunRaffle(name, email, phone, true);
    }
    else
        $('#modal').modal('show');
});

function centerRaffle(){
    let raffleContainer = $('.raffle-container'),
        $window = $(window),
        containerOffsetTop = raffleContainer.offset().top,
        containerHeight = raffleContainer.outerHeight(),
        windowHeight = $window.height();

    $window.scrollTop( containerOffsetTop + (containerHeight/2) - (windowHeight/2) )
}

$('#visitorInfo').on('submit', function(e){
    e.preventDefault();
    let name = $('#name').val(),
        email = $('#email').val(),
        phone = $('#phone').val();

    if ( $.trim(name) === '' ) {
        alert('name cannot be blank')
    }
    if ( $.trim(email) === '' ){
        alert('email cannot be blank')
    }

    if ( $.trim(phone) === '' ){
        alert('phone cannot be blank')
    }
    window.localStorage.setItem('name', name);
    window.localStorage.setItem('email', email);
    window.localStorage.setItem('phone', phone);

    $("#loading").show();
    $("#formBody").hide();


    if($('.giftwrapper').length){
        centerRaffle();
    }
    const isLastThreeDay = $('#isLastThreeDays').val();
    getGiftAndRunRaffle(name, email, phone, !!isLastThreeDay);
});

function getGiftAndRunRaffle(name, email, phone, lastThreeDays = false)
{
    $.ajax({
        url: lastThreeDays? '/participants': '/gifts',
        method: 'POST',
        dataType: 'json',
        data: { name: name, email: email, phone: phone, "_token": document.head.querySelector('meta[name="csrf-token"]').content },
        success(data){
            if(data.status){
                $.ajax({
                    url: '/notify-winner',
                    method: 'POST',
                    dataType: 'json',
                    async: true,
                    data: { name: name, email: email, phone: phone, winner_id: data.winner_id, gift_id: data.gift.id, "_token": document.head.querySelector('meta[name="csrf-token"]').content },
                    success(notify){
                        console.log(notify);
                    },
                    error(xhr, status, error) {

                    }
                });
                setTimeout(function(){
                    $("#loading").hide();
                    $("#formBody").show();
                    $('#modal').modal('hide');
                    $('#raffle').hide();

                    if(!lastThreeDays)
                    {
                        moveRaffle(data);
                    }
                    else{
                        swal({
                            title: 'Congratulations!',
                            text: data.message,
                            html: `<div id="swal2-content" class="mb-3" style="display: block;">${data.message}</div>
                                    <small class="text-muted font-weight-light font-italic">
                                        Sujeto a verificación de cumplimiento con las términos y condiciones de la promoción.
                                    </small>
                            `,
                            type: 'success',
                            confirmButtonText: '¡Haz click!',
                            confirmButtonColor: 'white',
                            customClass: lastThreeDays? 'has-no-header success-modal': 'success-modal'
                        });
                    }
                }, 10);
            }
            $(this).prop('disabled', false);
        },
        error(xhr, status, error) {
            const message = xhr.responseJSON.message;
            const couponLink = xhr.responseJSON.coupon;
            const hideHeaderImage = xhr.responseJSON.hideHeaderImage;
            $('#modal').modal('hide');
            $("#loading").hide();
            $("#formBody").show();
            const data = { status: false, coupon: couponLink, hideHeaderImage: hideHeaderImage };

            if(!lastThreeDays)
            {
                moveRaffle(data, message);
            }
            else{
                swal({
                    title: 'Error',
                    text: message,
                    html: `<div id="swal2-content" class="mb-3" style="display: block;">${message}</div>
                                    <small class="text-muted font-weight-light font-italic">
                                        Sujeto a verificación de cumplimiento con las términos y condiciones de la promoción.
                                    </small>
                            `,
                    type: 'error',
                    confirmButtonText: 'Ver cupón',
                    confirmButtonColor: '#e3342f',
                    customClass: 'error-modal'
                });
            }

            $(this).prop('disabled', false);
        }
    });
}

$('#modal').on('hide.bs.modal', function (e) {
    $('#raffle').show();
});

$('#modal').on('shown.bs.modal', function (e) {
    $('#raffle').hide();
});

function moveRaffle(data, message=''){
    let randomgift = Math.floor((Math.random() * 4) + 1);
    let distance = giftamount * gw * cycle + containercenter + (randomgift*gw) - giftcenter;

    $( ".giftwrapper" ).css({left: "0"});

    $('.giftwrapper').animate({left: "-="+distance}, 3000, function(){
        // alert('You Won Gift' + randomgift);
        // $('#raffle').show();

        let swalOptions = {
            title: data.status? 'Congratulations!': 'Error!',
            // text: message || data.message,
            // text: message || 'you have won ' + data.gift.gift,
            type: data.status? 'success': 'error',
            confirmButtonText: data.status? '¡Haz click!': 'Ver cupón',
            confirmButtonColor: data.status? 'white': '#e3342f',
            customClass: data.status? 'success-modal': 'error-modal' + (data.hideHeaderImage? ' hide-header-image': '')
        };

        if(data.gift)
        {
            swalOptions.html = `<div id="swal2-content" class="mb-3" style="display: block;">${message || data.message}</div>
                    <div>
                        <img src="/images/${data.gift.img}" alt="${data.gift.gift}" width="150">
                    </div>
                    <div>
                        <small class="text-muted font-weight-light font-italic">
                            Sujeto a verificación de cumplimiento con las términos y condiciones de la promoción.
                        </small>
                    </div>
            `;
        }else{
            swalOptions.html = `<div id="swal2-content" class="mb-3" style="display: block;">${message || data.message}</div>
                    <div>
                        <small class="text-muted font-weight-light font-italic">
                            Sujeto a verificación de cumplimiento con las términos y condiciones de la promoción.
                        </small>
                    </div>
            `;
        }

        swal(swalOptions).then((d) => {
            window.location = data.coupon
        });

        if(data.gift){
            // console.log(data.gift.img, randomgift);
            $(".gift p:contains('Gift " + randomgift + "')").prev().attr('src', `/images/${data.gift.img}`);
        }

    });
}