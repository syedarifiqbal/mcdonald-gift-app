<?php

namespace App;

use App\Models\Gift;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Participant extends Model
{
    use Notifiable;

    public static function getCurrentParticipant()
    {
        $now = now()->format('Y-m-d H:i:s');
        // \DB::listen(function ($q) { dd($q); });
         $participant = static::where(function($q){
            $q->orWhere('email', request('email'))
                ->orWhere('phone', request('phone'))
//                ->orWhere('ip', request()->ip())
            ;
         })->whereRaw("'$now' <= `blocked_till`")->first();

        return !!$participant;
    }

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }
}
