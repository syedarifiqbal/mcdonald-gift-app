<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\Twilio;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client;

class GiftWon extends Notification implements ShouldQueue
{
    use Queueable;

    public $winner;
    public $gift;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($gift, $winner)
    {
        $this->gift = $gift;
        $this->winner = $winner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $sid = "ACc6859bf5aba2539262ffd2db192cbe1a"; // Your Account SID from www.twilio.com/console
        $token = "f1f9604973e24873c24d8626d87d5091"; // Your Auth Token from www.twilio.com/console
        try {
            $client = new Client($sid, $token);
            $message = $client->messages->create(
                $notifiable->phone, // Text this number
                array(
                    'from' => '+17874190024', // From a valid Twilio number
                    'body' => "¡Felicidades {$notifiable->name}! Has ganado {$notifiable->gift->gift}. Favor de llamar al (787) 248-0278 o escribir a yarelis.perez@tbwasanjuan.com coordinar el recogido. *Sujeto a verificación de cumplimiento con las términos y condiciones de la promoción."
                )
            );
        } catch (ConfigurationException $e) {
            // dd($e->getMessage());
        }

        return (new MailMessage)->from('noreply@navidapppr.com', "McDonald's NavidApp")
            ->subject('¡Ganaste el premio de hoy!')
            ->view('mails.notification', ['winner' => $this->winner, 'gift' => $this->gift]);
    }

    public function toTwilio($notifiable)
    {


        return (new TwilioSmsMessage())
            ->content("¡Felicidades {$notifiable->name}! Has ganado {$notifiable->gift->gift}. Favor de llamar al (787) 248-0278 o escribir a yarelis.perez@tbwasanjuan.com coordinar el recogido. *Sujeto a verificación de cumplimiento con las términos y condiciones de la promoción.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
