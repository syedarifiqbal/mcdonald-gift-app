<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Loser extends Model
{
    protected $guarded = [];

    public static function recordLoser($gift, $request)
    {
        if(static::getTodayParticipant())
            return;

        static::create([
            'day' => $gift->day,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'ip' => $request->ip(),
        ]);
    }

    public static function getTodayParticipant()
    {
        $today = now()->format('Y-m-d');
        return static::where('email', request('email'))
                ->where('phone', request('phone'))
                ->where('ip', request()->ip())
            ->whereRaw("'$today' = DATE_FORMAT(created_at, '%Y-%m-%d')")->first();
    }
}
