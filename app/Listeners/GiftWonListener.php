<?php

namespace App\Listeners;

use App\Events\GiftWon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GiftWonListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GiftWon $event
     * @return void
     * @throws \SendGrid\Mail\TypeException
     */
    public function handle(GiftWon $event)
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("noreply@naviapppr.com", "Navi App pr");
        $email->setSubject("Sending with SendGrid is Fun");
        $email->addTo("test@example.com", "Example User");
        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
        );
        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        $event->winner->notify(new \App\Notifications\GiftWon($event->gift, $event->winner));
//        dd($event->gift->toArray(), $event->winner->toArray());
    }
}
