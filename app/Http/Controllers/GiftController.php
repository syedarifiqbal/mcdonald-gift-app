<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Events\GiftWon;
use App\Loser;
use App\Models\Gift;
use App\Models\Winner;
use App\Participant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GiftController extends Controller
{
    public function check(Request $request)
    {
        $gift = Gift::currentGift();
        $alreadyMovedCurrentHourRaffle = Winner::getCurrentWinner();
        $todayWinner = Winner::getTodayWinner();
        $coupon = Coupon::inRandomOrder()->first();
        $giftAvailable = Gift::isGiftAvailable();

        $todayLoser = Loser::getTodayParticipant();
        $hideHeaderImage = $gift && $gift->day === 12;

        if( !$gift )
        {
            return $this->returnResponse([
                'status'=> false,
                'message' => "Mientras tanto, disfruta esta deliciosa oferta."
                /*old message - Please try again tomorrow.*/,
                'coupon' => $coupon->link,
                'hideHeaderImage' => $hideHeaderImage
            ], 401);
        }

        if( ! $giftAvailable )
        {
            Loser::recordLoser($gift, $request);
            return $this->returnResponse([
                'status'=> false,
                'message' => "Mientras tanto, disfruta esta deliciosa oferta."
                /*old message - "They are no gifts left. Try next hour.*/,
                'coupon' => $coupon->link,
                'hideHeaderImage' => $hideHeaderImage
            ], 401);
        }

        if( $todayLoser )
        {
            Loser::recordLoser($gift, $request);
            return $this->returnResponse([
                'status'=> false,
                'message' => "Mientras tanto, disfruta esta deliciosa oferta.",
                'coupon' => $coupon->link,
                'hideHeaderImage' => $hideHeaderImage
                /*old message - Meanwhile, here you have a delicious discount."*/
            ], 401);
        }

        if( $todayWinner )
        {
            Loser::recordLoser($gift, $request);
            return $this->returnResponse([
                'status'=> false,
                'message' => "Ya ganaste hoy. Puedes intentarlo nuevamente mañana. Mientras tanto, disfruta esta deliciosa oferta.",
                'coupon' => $coupon->link,
                'hideHeaderImage' => $hideHeaderImage
                /*old message - You already won today. You can try again tomorrow. Meanwhile, here you have a delicious discount."*/
            ], 401);
        }

        if( $todayWinner && $alreadyMovedCurrentHourRaffle )
        {
            Loser::recordLoser($gift, $request);
            return $this->returnResponse([
                'status'=> false,
                'message' => "Mientras tanto, disfruta esta deliciosa oferta."
                /*old message - You have not won the big price.*/,
                'coupon' => $coupon->link,
                'hideHeaderImage' => $hideHeaderImage
            ], 401);
        }

        if( $todayWinner && !$alreadyMovedCurrentHourRaffle )
        {
            Loser::recordLoser($gift, $request);
            return $this->returnResponse([
                'status'=> false,
                'message' => "Mientras tanto, disfruta esta deliciosa oferta.",
                'coupon' => $coupon->link,
                'hideHeaderImage' => $hideHeaderImage
                /*old message - You already have won today's gift. please try again tomorrow, But you have won a food coupon, please check your SMS and email*/
            ], 401);
        }

        $blockedTill = Carbon::now()->format('Y-m-d ') . $gift->time_to;

        $winner = Winner::forceCreate([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'ip' => $request->ip(),
            'blocked_till' => $blockedTill,
            'won_date1' => Carbon::now()->format('Y-m-d'),
            'won_date2' => Carbon::now()->format('Y-m-d'),
            'gift_id' => $gift->id
        ]);

        return $this->returnResponse([
            'status' => true,
            'gift' => $gift,
            'message' => "Revisa tu e-mail y mensajes de texto para más detalles sobre el recogido de tu premio.",
//            'message' => "Has ganado $gift->gift. Favor de verificar tu correo electrónico y tus textos para ver las instrucciones de cómo redimir.",
//            'message' => "&iexcl;Ganaste!" . $message,
            'coupon' => $coupon->link,
            'winner_id' => $winner->id,
        ], 200);
    }

    public function participant(Request $request)
    {
        $gift = Gift::where('date', now()->format('Y-m-d'))->first();
        $alreadyParticipatedCurrentHour = Participant::getCurrentParticipant();

        if( $alreadyParticipatedCurrentHour )
        {
            Loser::recordLoser($gift, $request);
            return $this->returnResponse([ 'status'=> false, 'message' => "Ya participaste en esta hora, intenta en la próxima."], 401);
            /*You already participated in this hour, try in the next one.*/
        }

        $blockedTill = now()->format('Y-m-d ') . now()->format('H:59:59');

        $winner = Participant::forceCreate([
            'date' => now(),
            'day' => $gift->day,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'ip' => $request->ip(),
            'blocked_till' => $blockedTill,
            'gift_id' => $gift->id
        ]);

        return $this->returnResponse([
            'status'=> true,
            'message' => "\"Estás participando por el premio de hoy\", puedes participar 1 vez por hora.",
            'gift' => $gift,
            'winner_id' => $winner->id
        ]);
        /*You are participating for today's prize, you can participate 1 time per hour.*/
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function notify(Request $request)
    {
        $gift = Gift::find($request->gift_id);

        if($gift->day > 9) return;

        $winner = Winner::find($request->winner_id);

        $winner->notify(new \App\Notifications\GiftWon($gift, $winner));

        return $this->returnResponse([ 'status'=> true, 'message' => 'Notified user successfully', 'gift' => $gift]);
    }

    protected function returnResponse($message, $code = 200)
    {
        return response($message, $code, ['Content-Type' => 'application/json']);
    }

    public function check2(Request $request)
    {
        $gift = Gift::currentGift();
        $alreadyMovedCurrentHourRaffle = Winner::getCurrentWinner();
        $todayWinner = Winner::getTodayWinner();

        $giftAvailable = Gift::isGiftAvailable();

        if( ! $giftAvailable )
        {
            return response([
                'status'=> false,
                'message' => ($todayWinner)? "Ya ganaste hoy.": "Favor de intentarlo mañana nuevamente."
                /*old message - You have already win!": "They are no gifts left. Try next hour.*/
            ], 401, ['Content-Type' => 'application/json']);
        }

        if( !$gift )
        {
            return response([
                'status'=> false,
                'message' => "Mientras tanto aquí tienes un delicioso descuento."
                /*old message - Please try again tomorrow.*/
            ], 401, ['Content-Type' => 'application/json']);
        }

        if( $todayWinner && $alreadyMovedCurrentHourRaffle )
        {
            return response([
                'status'=> false,
                'message' => "Mientras tanto aquí tienes un delicioso descuento."
                /*old message - You have not won the big price.*/
            ], 401, ['Content-Type' => 'application/json']);
        }

        if( $todayWinner && !$alreadyMovedCurrentHourRaffle )
        {
            return response([
                'status'=> false,
                'message' => "Mientras tanto aquí tienes un delicioso descuento."
                /*old message - You already have won today's gift. please try again tomorrow, But you have won a food coupon, please check your SMS and email*/
            ], 401, ['Content-Type' => 'application/json']);
        }

        $blockedTill = Carbon::now()->format('Y-m-d ') . $gift->time_to;

        $winner = Winner::forceCreate([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'ip' => $request->ip(),
            'blocked_till' => $blockedTill,
            'won_date1' => Carbon::now()->format('Y-m-d'),
            'won_date2' => Carbon::now()->format('Y-m-d'),
            'gift_id' => $gift->id
        ]);

        // $winner->notify(new \App\Notifications\GiftWon($gift, $winner));

        $message = '';

        if($gift->tomorrowStartTime())
        {
            $message = ' Please visit us tomorrow at {$gift->tomorrowStartTime()} AM to win more stuff. We will be contacting you to redeem your gift';
        }

        return response([
            'status' => true,
            'gift' => $gift,
            'message' => "Has ganado $gift->gift. Favor de verificar tu correo electrónico y tus textos para ver las instrucciones de cómo redimir."
        ], 200, ['Content-Type' => 'application/json']);
    }

}
