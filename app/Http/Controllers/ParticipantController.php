<?php

namespace App\Http\Controllers;

use App\Loser;
use App\Models\Gift;
use App\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    function index()
    {
        $gifts = Gift::with('winners.gift')->get();

        $losers = Loser::all()->groupBy('day');

        $winners = [];
        foreach ($gifts as $gift)
        {
            $winners[$gift->day][] = $gift->winners;
        }

        $data = [
            'records' => $winners,
            'losers' => $losers
        ];
        return view('participants.index', $data);
    }

    function index_backup()
    {
        $gifts = Gift::with('winners.gift', 'participants')->get();

        $losers = Loser::all()->groupBy('day');

        $winners = [];
        foreach ($gifts as $gift)
        {
            $winners[$gift->day][] = $gift->winners->isEmpty()? $gift->participants: $gift->winners;
        }

        $data = [
            'records' => $winners,
            'losers' => $losers
        ];
        return view('participants.index', $data);
    }
}
