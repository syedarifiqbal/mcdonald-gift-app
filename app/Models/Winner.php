<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Winner extends Model
{
    use Notifiable;

    protected $guarded = [];

    public static function getCurrentWinner()
    {
        $now = now()->format('Y-m-d H:i:s');
        // DB::listen(function($q){ dd($q); });
        return !static::where(function($q){
            // $q->orWhere('name', request('name'))
            $q->orWhere('email', request('email'))
                ->orWhere('phone', request('phone'))
//                ->orWhere('ip', request()->ip())
            ;
        })->whereRaw("'$now' >= `blocked_till`")->first();
    }

    public static function getTodayWinner()
    {
        $today = Carbon::now()->format('Y-m-d');
        return static::where(function($q){
            // $q->orWhere('name', request('name'))
            $q->orWhere('email', request('email'))
                ->orWhere('phone', request('phone'))
//                ->orWhere('ip', request()->ip())
            ;
        })->whereRaw("'$today' = DATE_FORMAT(created_at, '%Y-%m-%d')")->first();
    }

    public static function countWinnersForCurrentHour()
    {
        $start = now()->format('Y-m-d H:00:00');
        $end = now()->format('Y-m-d H:59:00');

        return static::whereRaw("created_at BETWEEN '$start' AND '$end'")
            ->get()->count();
    }

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public function routeNotificationForTwilio()
    {
        return $this->phone;
    }
}
