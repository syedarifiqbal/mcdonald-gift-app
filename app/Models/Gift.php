<?php

namespace App\Models;

use App\Loser;
use App\Participant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Gift extends Model
{
    protected $casts = [ 'date' => 'date' ];
    protected static $available_hours = ['09', '10', '11', '12', '13', '14', '15', '15', '16', '17', '18', '19', '20', '21', '22'];

    /**
     * @return mixed
     */
    public static function currentGift()
    {
        $now = now()->format('Y-m-d H:i:s');
        return static::whereRaw("'$now' >= CONCAT(date, ' ', time_from) AND '$now' < CONCAT(date, ' ', time_to)")
            ->first();
    }

    public static function isGiftAvailable()
    {
        $hour = now()->format('H');
        $column = 'no_gifts_' . $hour;

        if(!in_array($hour, self::$available_hours)) return false;
        $gift = static::select($column)->where($column, '>', 0)->where('date', now()->format('Y-m-d'))->first();
        if(!$gift)
            return false;
        $allowedGift = $gift->{$column};
        $winners = Winner::countWinnersForCurrentHour();
        return $winners < $allowedGift;
    }

    public function isExpiredClass()
    {
        return $this->date < today() ? ' expired-days': '';
    }

    public function nextAvailableHour()
    {
        $hour = now()->format('H');
        if(!in_array($hour, self::$available_hours)) return false;
    }

    public static function tomorrowStartTime()
    {
        $date = now()->modify('+1 day')->format('Y-m-d');
        $gift = self::where('date', $date)->first();
        if( ! $gift )
        {
            return false;
        }
        foreach (static::$available_hours as $hours)
        {
            if($gift->{'no_gifts_'.$hours})
            {
                $am = ' AM';
                if($hours>12)
                {
                    $hours /= 2;
                    $am = ' PM';
                }
                return $hours . $am;
            }
        }
        return false;
    }

    public static function getLastTime()
    {
        $date = now()->format('Y-m-d');
        $gift = self::where('date', $date)->first();
        if( ! $gift )
        {
            return false;
        }

        foreach (static::$available_hours as $hours)
        {
            if($gift->{'no_gifts_'.$hours})
            {
                $am = ' AM';
                if($hours>12)
                {
                    $hours /= 2;
                    $am = ' PM';
                }
                return $hours . $am;
            }
        }
        return false;
    }

    public static function isTimeEnded()
    {
        $date = now()->format('Y-m-d');
        $gift = self::where('date', $date)->first();

        if( ! $gift ) { return true; }

        $giftEndTime = now()->setTimeFromTimeString(substr($gift->date, 0, 10) . ' ' . $gift->today_end_time);

        return now() > $giftEndTime;
    }

    public function winners()
    {
        return $this->hasMany(Winner::class);
    }

    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    public function losers()
    {
        return $this->hasMany(Loser::class);
    }
}
