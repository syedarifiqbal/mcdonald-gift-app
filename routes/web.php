<?php

Route::get('/', function () {

    $gifts = \App\Models\Gift::all();

    $upcomingGift = null;

    $row1 = $gifts->whereIn('id', [1, 2]);
    $row2 = $gifts->whereIn('id', [15, 16, 17]);
    $row3 = $gifts->whereIn('id', [18, 19, 20, 21]);
    $row4 = $gifts->whereIn('id', [22, 23, 24]);
    $timeEnded = now() > now()->setTime(20, 0,0) || now() < now()->setTime(10, 0,0);
    $todayGift = $gifts->where('date', now()->format('Y-m-d 00:00:00'))->first();
    $lastGift = $row4->last();

    if($todayGift)
    {
        $startTime = now()->setTime(10, 0);
        if(now() < $startTime)
            $upcomingGift = $todayGift;
        else
            $upcomingGift = \App\Models\Gift::where('day', $todayGift->day+1)->first();
    }

    return view('welcome', [
        'row1' => $row1,
        'row2' => $row2,
        'row3' => $row3,
        'row4' => $row4,
        'todayGift' => $todayGift,
        'upcomingGift' => $upcomingGift,
        'lastGift' => $lastGift,
        'timeEnded' => $timeEnded
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/gifts', 'GiftController@check')->name('gifts.check');
Route::post('/participants', 'GiftController@participant')->name('gifts.participant');
Route::get('/participants', 'ParticipantController@index')->name('gifts.participant')->middleware('auth');
Route::post('/notify-winner', 'GiftController@notify')->name('winner.notify');

Route::get('/migrate', function(){
    echo \Illuminate\Support\Facades\Artisan::call('migrate:refresh', ['--seed' => true]);
});

Route::get('/clear', function(){
    echo \Illuminate\Support\Facades\Artisan::call('cache:clear');
    echo \Illuminate\Support\Facades\Artisan::call('config:cache');
});
