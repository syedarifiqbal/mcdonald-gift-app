<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $gifts = [];

    public function __construct()
    {
        $date = now()->setDate(2018, 11, 30);

        $this->gifts = [
            [
                'gift' => 'Come GRATIS por 1 año',
                'description' => 'Come gratis por 1 año',
                'img' => 'daily-gifts/day-01.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 1st day
                'day' => '1',
                'time_from' => '10:00',
                'time_to' => '19:00',
                'no_gifts_10' => '2',
                'no_gifts_11' => '2',
                'no_gifts_12' => '2',
                'no_gifts_13' => '2',
                'no_gifts_14' => '2',
                'no_gifts_15' => '2',
                'no_gifts_16' => '2',
                'no_gifts_17' => '2',
                'no_gifts_18' => '2',
                'no_gifts_19' => '2',
                'tomorrow_start_at' => '09 AM',
                'today_end_time' => '19:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Tundra Bottle',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-9am.jpg',
                'date' => $date->modify("+1 day")->format('Y-m-d'),
                'day' => '2',
                'time_from' => '09:00',
                'time_to' => '10:00',
                'no_gifts_09' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Big Mac Wrap Can Cooler and Fry Box Beach Towel',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-10am.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '10:00',
                'time_to' => '11:00',
                'no_gifts_10' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Big Mac and Fry Box Pen Set and Big Mac Wrap Notebook',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-11am.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '11:00',
                'time_to' => '12:00',
                'no_gifts_11' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Fry Box Tote',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-12pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '12:00',
                'time_to' => '13:00',
                'no_gifts_12' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Compact Umbrella',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-1pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '13:00',
                'time_to' => '14:00',
                'no_gifts_13' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Red Coleman Cooler',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-2pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '14:00',
                'time_to' => '15:00',
                'no_gifts_14' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Big Mac Umbrella',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-3pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '15:00',
                'time_to' => '16:00',
                'no_gifts_15' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Big Mac Backpack',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-4pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '16:00',
                'time_to' => '17:00',
                'no_gifts_16' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Escapade Backpack',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-5pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '17:00',
                'time_to' => '18:00',
                'no_gifts_17' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Holiday Graphic Socks',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-6pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '18:00',
                'time_to' => '19:00',
                'no_gifts_18' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Fry Socks and Fry Box Tote',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-7pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '19:00',
                'time_to' => '20:00',
                'no_gifts_19' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Cork Arches Flat Bill Cap',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-8pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '20:00',
                'time_to' => '21:00',
                'no_gifts_20' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => "Articulo promocional de McDonald's",
                'description' => 'Red Mesh Travel Chair',
                'img' => 'daily-gifts/day-02.png',
                'hour_img' => 'individual-gift-images/day-02-9pm.jpg',
                'date' => $date->format('Y-m-d'),
                'day' => '2',
                'time_from' => '21:00',
                'time_to' => '22:00',
                'no_gifts_21' => '2',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '21:59:59',
            ],
            [
                'gift' => 'Boletos de Caribbean Cinemas',
                'description' => 'Boletos de Caribbean Cinemas',
                'img' => 'daily-gifts/day-03.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'),
                'day' => '3',
                'time_from' => '10:00',
                'time_to' => '19:00',
                'no_gifts_10' => '1',
                'no_gifts_11' => '1',
                'no_gifts_12' => '1',
                'no_gifts_13' => '1',
                'no_gifts_14' => '1',
                'no_gifts_15' => '1',
                'no_gifts_16' => '1',
                'no_gifts_17' => '1',
                'no_gifts_18' => '1',
                'no_gifts_19' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '19:59:59',
            ],
            [
                'gift' => '$100 en efectivo',
                'description' => '$100 en efectivo',
                'img' => 'daily-gifts/day-04.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 4th day
                'day' => '4',
                'time_from' => '10:00',
                'time_to' => '19:00',
                'no_gifts_10' => '1',
                'no_gifts_11' => '1',
                'no_gifts_12' => '1',
                'no_gifts_13' => '1',
                'no_gifts_14' => '1',
                'no_gifts_15' => '1',
                'no_gifts_16' => '1',
                'no_gifts_17' => '1',
                'no_gifts_18' => '1',
                'no_gifts_19' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '19:59:59',
            ],
            [
                'gift' => 'Bocinas JBL Flip',
                'description' => 'Bocinas JBL Flip 4',
                'img' => 'daily-gifts/day-05.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 5th day
                'day' => '5',
                'time_from' => '10:00',
                'time_to' => '20:00',
                'no_gifts_10' => '1',
                'no_gifts_12' => '1',
                'no_gifts_14' => '1',
                'no_gifts_16' => '1',
                'no_gifts_18' => '1',
                'no_gifts_20' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '20:59:59',
            ],
            [
                'gift' => 'Membresía Netflix Básica de 1 año',
                'description' => 'Membresía Netflix Básica de 1 año',
                'img' => 'daily-gifts/day-06.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 6th day
                'day' => '6',
                'time_from' => '09:00',
                'time_to' => '21:00',
                'no_gifts_10' => '1',
                'no_gifts_12' => '1',
                'no_gifts_14' => '1',
                'no_gifts_16' => '1',
                'no_gifts_18' => '1',
                'no_gifts_20' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '20:59:59',
            ],
            [
                'gift' => 'Membresía de Spotify Premium de 1 año',
                'description' => 'Membresía de Spotify Premium de 1 año',
                'img' => 'daily-gifts/day-07.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 7th day
                'day' => '7',
                'time_from' => '09:00',
                'time_to' => '21:00',
                'no_gifts_10' => '1',
                'no_gifts_12' => '1',
                'no_gifts_14' => '1',
                'no_gifts_16' => '1',
                'tomorrow_start_at' => '12 PM',
                'today_end_time' => '16:59:59',
            ],
            [
                'gift' => 'GoPro Hero 7',
                'description' => 'GoPro Hero 7',
                'img' => 'daily-gifts/day-08.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 8th day
                'day' => '8',
                'time_from' => '09:00',
                'time_to' => '21:00',
                'no_gifts_12' => '1',
                'no_gifts_14' => '1',
                'no_gifts_16' => '1',
                'tomorrow_start_at' => '12 PM',
                'today_end_time' => '16:59:59',
            ],
            [
                'gift' => 'Apple Watch Series 3',
                'description' => 'Apple Watch Series 3',
                'img' => 'daily-gifts/day-09.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 9th day
                'day' => '9',
                'time_from' => '09:00',
                'time_to' => '21:00',
                'no_gifts_12' => '1',
                'no_gifts_16' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '16:59:59',
            ],
            [
                'gift' => '4 taquillas arena para Bad Bunny',
                'description' => '4 taquillas arena para Bad Bunny',
                'img' => 'daily-gifts/day-10.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'), // 10th day
                'day' => '10',
                'time_from' => '10:00',
                'time_to' => '19:00',
                'no_gifts_12' => '1',
                'no_gifts_14' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '19:59:59',
            ],
            [
                'gift' => 'Certificado para 4 personas en crucero',
                'description' => 'Certificado para 4 personas en crucero',
                'img' => 'daily-gifts/day-11.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'),
                'day' => '11',
                'time_from' => '10:00',
                'time_to' => '19:00',
                'no_gifts_12' => '1',
                'tomorrow_start_at' => '10 AM',
                'today_end_time' => '19:59:59',
            ],
            [
                'gift' => 'Viaje para 4 personas a Disney',
                'description' => 'Viaje para 4 personas a Disney',
                'img' => 'daily-gifts/day-12.png',
                'date' => $date->modify("+1 day")->format('Y-m-d'),
                'day' => '12',
                'time_from' => '10:00',
                'time_to' => '19:00',
                'no_gifts_12' => '1',
                'today_end_time' => '19:59:59',
            ]
        ];
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \App\Models\Gift::truncate();
        foreach ($this->gifts as $gift) {
            \App\Models\Gift::forceCreate($gift);
        }
        \App\User::forceCreate([
            "name" => "Admin",
            "password" => bcrypt('N@v1aPp321!'),
            "email" => "info@naviapppr.com",
        ]);
        foreach ($this->getCoupons() as $coupon)
        {
            \App\Coupon::forceCreate($coupon);
        }
    }

    protected  function getCoupons()
    {
        return [
            [
                'title' => 'Combo Bacon Triple a $5 IVU incluido',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/595a5ffb12c3bfc2f1e1e64d?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => 'Smoothie 12oz a $1.99',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/595a5ffb12c3bfc2f1e1e64d?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => '20 McNuggets® a $4',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/595a5ffb12c3bfc2f1e1e64d?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => 'Combo Mallorca Jamón y Queso a $2.99',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/5bf2fcbfcd50e9ea03d09412?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => '2 Combos Big Mac® a $7.99',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/595a5ffb12c3bfc2f1e1e64d?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => 'Combo McDuo® y McChicken Jr.® a $5',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/5bf2fd8ccd50e9ea03d12b47?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => 'Avena + Café a $2.99',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/5bf2fdd6a9cbf8de0353a709?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => '2 Sundaes a $2',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/5bf2fe15cd50e9ea03d1a078?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => '2 combos McBacon® por $7',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/5bf2fe84cd50e9ea03d215a9?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
            [
                'title' => 'Combo burrito desayuno por $2.99',
                'link' => 'https://d2eay.app.goo.gl/?link=http://cupones.mcdonalds.com.pr/pr/campaign/5bf2fec8a9cbf8de03541c3a?country%3DPR&apn=com.mcdo.mcdonalds&isi=1114009187&ibi=com.mcdo.mcdonalds&dfl=http://www.mcdonalds.com.pr/apps&afl=http://www.mcdonalds.com.pr/apps&ifl=http://www.mcdonalds.com.pr/apps&'
            ],
        ];
    }
}
