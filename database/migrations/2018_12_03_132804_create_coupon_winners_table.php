<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_winners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_id');
            $table->date('date');
            $table->integer('day');
            $table->integer('no_gifts_09')->default(0);
            $table->integer('no_gifts_10')->default(0);
            $table->integer('no_gifts_11')->default(0);
            $table->integer('no_gifts_12')->default(0);
            $table->integer('no_gifts_13')->default(0);
            $table->integer('no_gifts_14')->default(0);
            $table->integer('no_gifts_15')->default(0);
            $table->integer('no_gifts_16')->default(0);
            $table->integer('no_gifts_17')->default(0);
            $table->integer('no_gifts_18')->default(0);
            $table->integer('no_gifts_19')->default(0);
            $table->integer('no_gifts_20')->default(0);
            $table->integer('no_gifts_21')->default(0);
            $table->integer('no_gifts_22')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_winners');
    }
}
